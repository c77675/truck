#!/bin/bash

[ "$V" ] &&
	set -x

: ${CC:='c++'}
: ${LD:="$CC"}
: ${SE:='.cpp'}

CFLAGS="$CFLAGS -Wall -Wfatal-errors -std=c++17"
LDFLAGS="$LDFLAGS -Wl,--as-needed"

: ${ARCH:="$(uname -m)"}
: ${OS:="$(uname -o)"}

case "$OS" in
	"Windows_NT")
		: ${BINSUFFIX:='.exe'}
		: ${LIBSUFFIX:='.dll'}
	;;
	*)
		: ${BINSUFFIX:=''}
		: ${LIBSUFFIX:='.so'}
	;;
esac

retire()
{
	printf "$0: something is wrong.\n"
	exit 4
}

isnt_newer()
{
	local r
	r=$(find "$1" -prune -newer "$2" -print 2>/dev/null) &&
		[ "$r" != "$1" ]
}

cleanup()
{
	find "$2" -type f |
		while read -r f
		do
			sfn="${f%$4}"
			[ "$sfn" != "$f" ] &&
				sfn="$sfn$3"
			if [ ! -e "$1${sfn#$2}" ]
			then
				printf "rm -- \"$f\"\n"
				rm -- "$f"
			fi
		done
	find "$2" -type d |
		while read -r d
		do
			if [ ! -d "$1/${d#$2}" ]
			then
				printf "rmdir -- \"$d\"\n"
				rmdir -- "$d"
			fi
		done
}

compile()
{
	[ -d "$1" ] ||
		return 0
	x=2
	LF=''
	[ -r "$1/CFLAGS" ] &&
		read -r LF < "$1/CFLAGS"
	CFLAGS="$CFLAGS $LF"

	[ -d "$2" ] ||
		mkdir -p "$2" 1>/dev/null

	if ls -d "$1"/*/ 2>/dev/null 1>&2
	then
		for sd in "$1"/*/
		do
			sd="${sd%/}"
			dn="${sd#$1/}"
			case "$dn" in
				"arch")
					[ -d "$sd/$ARCH" ] &&
						(compile "$sd/$ARCH" "$2/$dn/$ARCH")
				;;
				"os")
					[ -d "$sd/$OS" ] &&
						(compile "$sd/$OS" "$2/$dn/$OS")
				;;
				*)
					(compile "$sd" "$2/$dn")
				;;
			esac
			x=$?
			[ $x -eq 4 ] &&
				exit 4
		done
	fi

	if ls -d "$1"/*"$SE" 2>/dev/null 1>&2
	then
		for sf in "$1"/*"$SE"
		do
			fn="${sf#$1/}"
			of="$2/${fn%$SE}.o"
			isnt_newer "$sf" "$of" &&
				continue
			cl="'$CC' -c -fdebug-prefix-map=./='$1/' $CFLAGS '$sf' -o '$of'"
			printf "$cl\n"
			eval $cl ||
				exit 4
			touch -cr "$sf" "$of"
			x=0
		done
	fi
	[ $x -eq 0 ] &&
		touch -c "$2"

	CFLAGS="${CFLAGS% $LF}"
	[ -r "$1/LDFLAGS" ] &&
		cp -f "$1/LDFLAGS" "$2/LDFLAGS"
	return $x
}

link()
{
	[ -d "$1" ] ||
		return 0
	[ -d "$2" ] ||
		mkdir -p "$2" 2>/dev/null 1>&2

	if ls -d "$1"/*/ 2>/dev/null 1>&2
	then
		for sd in "$1"/*/
		do
			sd="${sd%/}"
			LF0=''
			LF1=''
			[ -r "$sd/LDFLAGS" ] &&
			{
				read -r LF0
				read -r LF1
			} < "$sd/LDFLAGS"
			dn="${sd#$1/}"
			of="$2/$dn"
			isnt_newer "$sd" "$of" &&
				continue
			sfs=$(find "$sd" -type f -name '*.o' -printf "'%p' ")
			[ "$sfs" ] ||
				continue
			cl="'$LD' $LDFLAGS $LF0 $sfs $LF1 -o '$of'"
			printf "$cl\n"
			eval $cl ||
				exit 4
			touch -cr "$sd" "$of"
		done
	fi
}

main()
{
	[ -d "obj" ] &&
		(cleanup "src" "obj" "$SE" ".o")

	for d in "lib" "bin"
	do
		(compile "src/$d" "obj/$d")
		[ $? -eq 4 ] &&
			retire
	done
	for d in "lib" "bin"
	do
		(link "obj/$d" "img/$d")
		[ $? -eq 4 ] &&
			retire
	done

	[ -d "img/lib" ] &&
		(cleanup "obj/lib" "img/lib" "/" "$LIBSUFFIX")
	[ -d "img/bin" ] &&
		(cleanup "obj/bin" "img/bin" "/" "$BINSUFFIX")

	[ -d "src/share" ] &&
		cp -rf "src/share" "img/"
	:
}

main

# vim:set ts=4 sts=4 sw=0 :
