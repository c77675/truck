/* include {{{*/
#include <agar/core.h>
#include <agar/gui.h>

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <queue>

#include "shared.hpp"
#include "ldb.hpp"
#include "lui.hpp"


using namespace std;
/*}}}*/

int AG_TableAddRowQ(AG_Table *, queue<Sany> *);
void rowFn(AG_Event *);


Lui::Tab::Tab(Ldb *db, Lui *ui, string name)
	: db (db), ui (ui), tbl (NULL), name (name) {
	this->tab = AG_NotebookAdd(ui->nb, name.c_str(), AG_BOX_VERT);
	AG_SetStyle(this->tab->lbl, "font-family", "sans");
	AG_SetStyle(tab->lbl, "font-size", "120%");
	AG_SetStyle(tab, "selection-color",  "#aaffaa");
	this->top = AG_BoxNewHoriz(this->tab, AG_BOX_EXPAND);
	this->btm = AG_BoxNewHoriz(this->tab
		, AG_BOX_HFILL | AG_BOX_HOMOGENOUS);
	this->redraw();
	AG_SetEvent(this->tbl, "row-selected", rowFn, NULL);
	AG_ButtonNewFn(this->btm, 0, "Добавить запись", Lui::genForm
		, "%p(win),%p(tab),%i(act)", ui->win, this, (int) 'I');
	AG_ButtonNewFn(this->btm, 0, "Изменить запись", Lui::genForm
		, "%p(win),%p(tab),%i(act)", ui->win, this, (int) 'U');
	AG_ButtonNewFn(this->btm, 0, "Удалить запись", Lui::genForm
		, "%p(win),%p(tab),%i(act)", ui->win, this, (int) 'D');
}
Lui::Tab::~Tab() {
	AG_ObjectDelete(this->tab);
}

void
Lui::Tab::redraw() {
	Ldb::Stmt stmt(db, "SELECT * FROM", this->name, " ");
	queue<Scol> md;
	queue<Sany> d;
	int cc = stmt.result(&d, &md);
	if (md.empty()) {
		clog << "Lui:drawTable: no columns" << endl;
		return;
	}
	if (this->tbl) {
		AG_TableClear(this->tbl);
	} else {
		this->tbl = AG_TableNew(this->top
			, AG_TABLE_EXPAND | AG_TABLE_NOAUTOSORT);
		AG_TableSetColumnAction(this->tbl, 0);
		AG_TableAddCol(this->tbl, md.front().name.c_str()
			, "40px", NULL);
		for (md.pop(); !md.empty(); md.pop())
			AG_TableAddCol(this->tbl, md.front().name.c_str()
				, "80px", NULL);
	}
	for (; cc > 0; cc = stmt.result(&d, &md)) {
		AG_TableAddRowQ(this->tbl, &d);
	}
	AG_Redraw(this->tbl);
	AG_SetInt(this->tbl, "sel", -1);
}

void
rowFn(AG_Event *event) {
	int row = AG_INT(1);
	int sel = AG_TableGetCell(AG_TABLE_SELF(), row, 0)->data.i;
	AG_SetInt(AG_TABLE_SELF(), "sel", sel);
	clog << "row: " << row << " data: " << sel << endl;
}
