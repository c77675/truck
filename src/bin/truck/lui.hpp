
using namespace std;

class Lui {
protected:
	AG_Window *win;
	AG_Notebook *nb;
public:
	Lui(string);
	~Lui();
	int loop();
	static void genForm(AG_Event *);

	class Tab {
	public:
		Ldb *db;
		Lui *ui;
		AG_NotebookTab *tab;
		AG_Table *tbl;
		string name;
		AG_Box *top, *btm;
		friend void Lui::genForm(AG_Event *);
		Tab(Ldb *, Lui *, string);
		~Tab();
		void redraw();
	};

	class Form {
	public:
		Lui::Tab *tab;
		AG_Window *mwin;
		Ldb *db;
		const string name;
		char act;
		int sel;
	protected:
		AG_Box *cont;
		AG_Button *btn;
		AG_Window *win;
		queue<Sany> var;
		string actL;
		queue<char *> tofree;
		friend void Lui::genForm(AG_Event *);
		static void closeForm(AG_Event *);
		static void showForm(Lui::Form *);
		static void formAct(AG_Event *);
	public:
		Form();
		Form(Lui::Tab *, AG_Window *);
		~Form();
	};
};
