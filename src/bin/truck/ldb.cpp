/* include {{{*/
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <queue>

#include <sqlite3.h>

#include "shared.hpp"
#include "ldb.hpp"
/*}}}*/

using namespace std;


Ldb::Ldb(const char *path) {
	if (sqlite3_open_v2(
		path, &db, SQLITE_OPEN_READWRITE, NULL) == SQLITE_OK)
		return;
	if (sqlite3_open(path, &db) == SQLITE_OK) {
		init();
		return;
	}
	throw runtime_error("sqlite_open failed");
}

Ldb::~Ldb() {
	sqlite3_close(db);
}

void
Ldb::init() {
	const char sql[] = u8"BEGIN TRANSACTION;		\
CREATE	TABLE	"DRV"	(					\
№		INTEGER	PRIMARY KEY	NOT NULL	,	\
ФИО		TEXT			NOT NULL	,	\
Заметки		TEXT						\
);								\
CREATE	TABLE	"CAR"	(					\
№		INTEGER	PRIMARY KEY	NOT NULL	,	\
Госномер	TEXT					,	\
Пробег		INTEGER					,	\
Заметки		TEXT						\
);								\
CREATE	TABLE	"CUS"	(					\
№		INTEGER	PRIMARY KEY	NOT NULL	,	\
Имя		TEXT			NOT NULL	,	\
Заметки		TEXT						\
);								\
CREATE	TABLE	"RUN"	(					\
№		INTEGER	PRIMARY KEY	NOT NULL	,	\
Дата		TEXT					,	\
Тип		TEXT					,	\
Из		TEXT					,	\
В		TEXT					,	\
Заказчик	INTEGER					,	\
Водитель	INTEGER					,	\
Машина		INTEGER					,	\
Заметки		TEXT						\
);								\
COMMIT;";
	eval(sql);
}

void
Ldb::eval(string sql) {
	char *s;
	int r = sqlite3_exec(db, sql.c_str(), NULL, NULL, &s);
	if (r != SQLITE_OK) {
		clog << "Ldb:eval: " << r << ": " << s << endl;
		throw runtime_error(sqlite3_errmsg(this->db));
	}
}

Ldb::Stmt::Stmt(const Ldb *edb, string sql0, string t, string sql1)
	: db(edb->db), tbl(t) {
	string sql = sql0 + " " + t + " " + sql1 + ";";
	clog << "Ldb:Stmt: " << sql << endl;
	int r = sqlite3_prepare_v2(this->db, sql.c_str(), -1, &stmt, NULL);
	if (r != SQLITE_OK)
		throw runtime_error(sqlite3_errmsg(this->db));
}

Ldb::Stmt::~Stmt() {
	sqlite3_finalize(stmt);
}

int
Ldb::Stmt::setQueryArg(Sany name, Sany val) {
	int id, r;
	switch (name.t) {
	case Etype::INT:
		id = name.i;
		clog << "setarg: #" << id;
		break;
	case Etype::TEXT:
		id = sqlite3_bind_parameter_index(stmt, name.p);
		clog << "setarg: #" << id << " '" << name.p << "'";
		break;
	default:
		throw invalid_argument("arg1 is not int nor string");
	}
	if (!id)
		throw invalid_argument("parameter not found");
	clog << " t:" << val.t;
	switch (val.t) {
	case Etype::INT:
		clog << " d:" << val.i << endl;
		r = sqlite3_bind_int(stmt, id, val.i);
		break;
	case Etype::REAL:
		clog << " d:" << val.d << endl;
		r = sqlite3_bind_double(stmt, id, val.d);
		break;
	case Etype::TEXT:
		clog << " d:"	<< val.p << endl;
		r = sqlite3_bind_text(stmt, id, val.p, -1, NULL);
		break;
	default:
		return -1;
	}
	if (r != SQLITE_OK)
		throw runtime_error(sqlite3_errmsg(this->db));
	return 0;
}

int
Ldb::Stmt::result(queue<Sany> *d, queue<Scol> *md) {
	int r = sqlite3_step(stmt);
	if (r == SQLITE_BUSY)
		clog << "Ldb:result: sqlite busy err" << endl;
	if (r != SQLITE_DONE && r != SQLITE_ROW)
		throw runtime_error(sqlite3_errmsg(this->db));
	if (this->cc == 0)
		this->cc = sqlite3_column_count(stmt);
	if (this->col.empty())
		for (unsigned int i = 0; i < this->cc; i++) {
			Scol ci;
			ci.name = string(sqlite3_column_name(stmt, i));
			const char *p;
			sqlite3_table_column_metadata(this->db, "main"
				, this->tbl.c_str(), ci.name.c_str()
				, &p, NULL, &ci.nnul, &ci.pkey, &ci.ainc);
			this->col.push(ci);
			if (r != SQLITE_DONE || this->ict.size() == this->cc)
				continue;
			switch (p[0]) {
			case 'I':
				ict.push_back(SQLITE_INTEGER);
				break;
			case 'F':
				ict.push_back(SQLITE_FLOAT);
				break;
			case 'T':
			default:
				ict.push_back(SQLITE_TEXT);
				break;
			}
		}
	if (md != NULL)
		*md = this->col;
	if (this->ict.empty())
		for (unsigned int i = 0; i < this->cc; i++)
			this->ict.push_back(sqlite3_column_type(stmt, i));
	if (d == NULL)
		return this->cc;
	clog << "Ldb:Stmt:result: ";
	for (unsigned int i = 0; i < this->cc; i++) {
		Sany di = {};
		switch (this->ict[i]) {
		case SQLITE_INTEGER:
			di.t = Etype::INT;
			if (r == SQLITE_DONE)
				break;
			di.i = sqlite3_column_int(stmt, i);
			clog << "int " << di.i;
			break;
		case SQLITE_FLOAT:
			di.t = Etype::REAL;
			if (r == SQLITE_DONE)
				break;
			di.d = sqlite3_column_double(stmt, i);
			clog << "float " << di.d;
			break;
		case SQLITE_TEXT:
			di.t = Etype::TEXT;
			if (r == SQLITE_DONE)
				break;
			di.s = (const char *) sqlite3_column_text(stmt, i)
				? : "";
			clog << "str " << (char *) di.s;
			break;
		default:
			clog << "NUL ";
			di.t = Etype::TEXT;
			di.s = "";
		}
		d->push(di);
	}
	clog << endl;
	if (r == SQLITE_DONE)
		return 0;
	return this->cc;
}
