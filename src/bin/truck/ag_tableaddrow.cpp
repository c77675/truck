#include <agar/core.h>
#include <agar/gui.h>

#include <queue>
#include <iostream>

#include "shared.hpp"

using namespace std;

int
AG_TableAddRowQ(AG_Table *tbl, queue<Sany> *d) {
	if (d->empty()) {
		clog << "addrowq: empty data supplied" << endl;
		return 0;
	}
	AG_ObjectLock(tbl);
	AG_TableCell **cells = (AG_TableCell **) realloc(tbl->cells
		, (tbl->m + 1) * sizeof(AG_TableCell));
	cells[tbl->m] = (AG_TableCell *) malloc(tbl->n * sizeof(AG_TableCell));
	tbl->cells = cells;
	clog << "addrow: ";
	for (int i = 0; !d->empty(); i++, d->pop()) {
		AG_TableCell *c = &tbl->cells[tbl->m][i];
		AG_TableInitCell(tbl, c);
		clog << d->front().t << ":";
		switch (d->front().t) {
		case Etype::INT:
			c->type = AG_CELL_INT;
			strncpy(c->fmt, "%i", sizeof(c->fmt));
			c->data.i = d->front().i;
			clog << c->data.i;
			break;
		case Etype::REAL:
			c->type = AG_CELL_DOUBLE;
			strncpy(c->fmt, "%f", sizeof(c->fmt));
			c->data.f = d->front().d;
			clog << c->data.f;
			break;
		case Etype::TEXT:
			c->type = AG_CELL_STRING;
			strncpy(c->fmt, "%s", sizeof(c->fmt));
			strncpy(c->data.s, d->front().p ? : ""
				, sizeof(c->data.s));
			clog << c->data.s;
			break;
		case Etype::NUL:
		default:
			c->type = AG_CELL_NULL;
			c->fmt[0] = '\0';
			c->data.p = NULL;
			clog << "NUL";
		}
		clog << " ";
	}
	clog << endl;
	tbl->flags |= AG_TABLE_NEEDSORT;
	tbl->m++;
	AG_ObjectUnlock(tbl);
	return tbl->m;
}
