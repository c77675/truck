

#define DRV u8"Водители"
#define CAR u8"Машины"
#define CUS u8"Заказчики"
#define RUN u8"Рейсы"


enum Etype {
	INT, LONG, REAL, TEXT, NUL
};
struct Sany {
	Etype t;
	union {
		int i;
		long l;
		double d;
		float f;
		char *p;
		const char *s;
	};
};
struct Scol {
	std::string name;
	int nnul;
	int pkey;
	int ainc;
};
