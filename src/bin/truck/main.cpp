#include <agar/core.h>
#include <agar/gui.h>

#include <sqlite3.h>

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <queue>

#include "shared.hpp"
#include "ldb.hpp"
#include "lui.hpp"


using namespace std;


int
main()
{
	setlocale(LC_CTYPE, ".UTF-8");

	Lui ui("truck");
	Ldb db("t.db");

	Lui::Tab tabs[] = {{&db, &ui, DRV}, {&db, &ui, CAR}
		, {&db, &ui, CUS}, {&db, &ui, RUN}};

	ui.loop();
}
