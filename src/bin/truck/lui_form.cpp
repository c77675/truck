/* include {{{*/
#include <agar/core.h>
#include <agar/gui.h>

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <queue>

#include "shared.hpp"
#include "ldb.hpp"
#include "lui.hpp"


using namespace std;
/*}}}*/

static char *
trim(char *s) {
	size_t sc = strnlen(s, PAGE_SIZE - 1);
	s[sc] = '\0';
	char *bs = s;
	while (*bs == ' ' || *bs == '\t') {
		bs++;
		sc--;
	}
	for (sc--; sc > 0; sc--)
		if (bs[sc] != ' ' && bs[sc] != '\t')
			break;
	bs[++sc] = '\0';
	wchar_t ws[PAGE_SIZE];
	int n = mbstowcs(ws, bs, sc + 1);
	if (n <= 0) {
		bs[0] = '\0';
		return bs;
	}
	int i;
	for (i = 0; i < n; i++)
		if (!iswprint(ws[i]))
			ws[i] = L'_';
	wcstombs(s, ws, n + 1);
	return s;
}

void
Lui::genForm(AG_Event *event) {
	AG_Window *win = (AG_Window *) AG_PTR_NAMED("win");
	Lui::Tab *tab = (Lui::Tab *) AG_PTR_NAMED("tab");
	int sel = AG_GetInt(tab->tbl, "sel");
	char act = (char) AG_INT_NAMED("act");
	if (sel < 0 && act != 'I')
		return;
	Lui::Form *f = new Lui::Form(tab, win);
	f->sel = sel;
	f->act = act;
	clog << "genForm:" + f->name << endl;
	f->win = AG_WindowNew(AG_WINDOW_MODAL);
	AG_WindowAttach(win, f->win);
	AG_WindowMakeTransient(win, f->win);
	AG_WindowPin(win, f->win);
	f->cont = AG_BoxNewVert(f->win, AG_BOX_EXPAND);
	AG_BoxSetHorizAlign(f->cont, AG_BOX_RIGHT);
	switch (act) {
	case 'I':
		f->actL = "Добавить";
		break;
	case 'U':
		f->actL = "Изменить";
		break;
	case 'D':
		f->actL = "Удалить";
		break;
	default:
		throw logic_error("bad form act");
	}
	Lui::Form::showForm(f);
	string title = f->actL + " запись в таблице " + f->name;
	AG_WindowSetCaptionS(f->win, title.c_str());
	AG_WindowSetCloseAction(f->win, AG_WINDOW_IGNORE);
	AG_WindowSetGeometry(f->win, -1, -1, 640, -1);
	AG_WindowShow(f->win);
}

void
Lui::Form::closeForm(AG_Event *event) {
	Lui::Form *f = (Lui::Form *) AG_PTR_NAMED("form");
	AG_ObjectDetach(f->win);
	delete f;
}

Lui::Form::Form(Lui::Tab *tab, AG_Window *win)
	: tab(tab), mwin (win), db(tab->db), name(tab->name), tofree() {
	clog << "Lui:Form: " << this->name << endl;
}
Lui::Form::~Form() {
	for (; !this->tofree.empty(); this->tofree.pop())
		delete this->tofree.front();
}

void
Lui::Form::showForm(Lui::Form *f) {
	clog << "Lui:Form:" << f->act << ": " << f->name << endl;

	AG_Box *buttonbox = AG_BoxNewHoriz(f->win
		, AG_BOX_HFILL | AG_BOX_HOMOGENOUS);
	AG_Button *btOk = AG_ButtonNewFn(buttonbox, 0, f->actL.c_str()
		, Lui::Form::formAct, "%p(form)", f);
	AG_Button *btCn = AG_ButtonNewFn(buttonbox, 0, "Отмена"
		, Lui::Form::closeForm, "%p(form)", f);

	if (f->act == 'D') {
		AG_LabelNewS(f->cont, AG_LABEL_HFILL
			, (f->actL + " запись №" + to_string(f->sel)
			+ "?").c_str());
		return;
	}

	Ldb::Stmt stmt(f->db, "SELECT * FROM", f->name
		, f->act == 'U' ? "WHERE № = $SEL LIMIT 1" : "LIMIT 1");
	if (f->act == 'U')
		stmt.setQueryArg({.t = Etype::TEXT, .s = "$SEL"} /* BAD */
			, {.t = Etype::INT, .i = f->sel});
	queue<Scol> md;
	queue<Sany> d;
	AG_Textbox *lw = NULL;
	for (stmt.result(&d, &md); !md.empty(); md.pop(), d.pop()) {
		Sany di = d.front();
		Scol mdi = md.front();
		if (mdi.pkey == 1)
			continue;
		f->var.push(di);
		switch (di.t) {
		case Etype::INT: {
			string l = mdi.name;
			if (l.size() < 16)
				l.insert(0, 16 - l.size(), ' ');
			AG_Numerical *w = AG_NumericalNewInt(f->cont
				, AG_NUMERICAL_HFILL, NULL
				, l.c_str(), &(f->var.back().i));
			clog << "numeric: " << di.i << endl;
			AG_SetString(w, "name", mdi.name.c_str());
			break;
			}
		case Etype::REAL: {
			string l = mdi.name;
			if (l.size() < 16)
				l.insert(0, 16 - l.size(), ' ');
			AG_Numerical *w = AG_NumericalNewDbl(f->cont
				, AG_NUMERICAL_HFILL, NULL
				, l.c_str(), &(f->var.back().d));
			clog << "numeric: " << di.d << endl;
			AG_SetString(w, "name", mdi.name.c_str());
			break;
			}
		case Etype::NUL:
		case Etype::TEXT: {
			string l = mdi.name;
			if (l.size() < 16)
				l.insert(0, 16 - l.size(), ' ');
			AG_Textbox *w = AG_TextboxNewS(f->cont
				, AG_TEXTBOX_HFILL, l.c_str());
			lw = w;
			f->var.back().p = new char[PAGE_SIZE] {'\0'};
			f->tofree.push(f->var.back().p);
			AG_TextboxBindUTF8(w, f->var.back().p, PAGE_SIZE);
			AG_BindString(w, "value", f->var.back().p, PAGE_SIZE);
			if (f->act == 'U')
				strncpy(f->var.back().p, di.p, PAGE_SIZE);
			AG_SetString(w, "name", mdi.name.c_str());
			break;
			}
		default:
			throw runtime_error("unknown data type");
		}
	}
	if (lw) {
		AG_TextboxSizeHintLines(lw, 3);
		AG_TextboxSetWordWrap(lw, 1);
	}
}

void
Lui::Form::formAct(AG_Event *event) {
	Lui::Form *f = (Lui::Form *) AG_PTR_NAMED("form");
	clog << f->act << "Act: " << endl;

	if (f->act == 'D') {
		Ldb::Stmt stmt(f->db, "DELETE FROM", f->name, "WHERE № = $SEL");
		stmt.setQueryArg({.t = Etype::TEXT, .s = "$SEL"} /* BAD */
			, {.t = Etype::INT, .i = f->sel});
		stmt.result(NULL, NULL);
		AG_ObjectDetach(f->win);
		f->tab->redraw();
		delete f;
		return;
	}

	AG_Widget *w;
	string cn(""), ids(""), cs("");
	AG_ObjectLock(f->cont);
	int i = 1;
	char name[PAGE_SIZE];
	AGOBJECT_FOREACH_CHILD(w, f->cont, ag_widget) {
		AG_GetString(w, "name", name, sizeof(name));
		cn += cs + "'" + name + "'";
		if (f->act == 'U')
			cn += "=?" + to_string(i++);
		else
			ids += cs + "?" + to_string(i++);
		cs = ",";
	}
	Ldb::Stmt stmt(f->db, f->act == 'U' ? "UPDATE " : "INSERT INTO"
		, f->name, f->act == 'U'
		? " SET " + cn + " WHERE № = $SEL"
		: "(" + cn + ") VALUES(" + ids + ")");
	if (f->act == 'U')
		stmt.setQueryArg({.t = Etype::TEXT, .s = "$SEL"} /* BAD */
			, {.t = Etype::INT, .i = f->sel});
	i = 1;
	AGOBJECT_FOREACH_CHILD(w, f->cont, ag_widget) {
		AG_Variable *v = AG_AccessVariable(w, "value");
		Sany id = {.t = Etype::INT, .i = i++};
		if (v == NULL)
			continue;
		switch (v->type) {
		case AG_VARIABLE_INT:
			stmt.setQueryArg(id
				, {.t = Etype::INT, .i = v->data.i});
			break;
		case AG_VARIABLE_P_STRING:
			stmt.setQueryArg(id
				, {.t = Etype::TEXT, .p = trim(v->data.s)});
			break;
		default:
			clog << "formAct: " << i << " unknown type: "
				<< v->type << endl;
		}
		AG_UnlockVariable(v);
	}
	stmt.result(NULL, NULL);
	AG_ObjectUnlock(f->cont);
	AG_ObjectDetach(f->win);
	f->tab->redraw();
	delete f;
}
