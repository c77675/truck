/* include {{{*/
#include <agar/core.h>
#include <agar/gui.h>

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <queue>

#include "shared.hpp"
#include "ldb.hpp"
#include "lui.hpp"


using namespace std;
/*}}}*/

Lui::Lui(string name) {
	if (AG_InitCore(name.c_str(), AG_VERBOSE) < 0)
		throw runtime_error(AG_GetError());
	if (AG_InitGraphics("<OpenGL>") < 0)
		throw runtime_error(AG_GetError());
	AG_BindGlobalKey(AG_KEY_ESCAPE, AG_KEYMOD_ANY, AG_Quit);
	this->win = AG_WindowNewNamedS(AG_WINDOW_MAIN, name.c_str());
	AG_WindowSetCaptionS(this->win, name.c_str());
	AG_WindowSetMinSize(this->win, 640, 360);
	AG_SetStyle(win, "font-family",      "sans");
	AG_SetStyle(win, "font-size",        "14");
	AG_SetStyle(win, "background-color", "#ffffff");
	AG_SetStyle(win, "high-color",       "#000000");
	AG_SetStyle(win, "low-color",        "#000000");
	AG_SetStyle(win, "line-color",       "#000000");
	AG_SetStyle(win, "line-color#hover", "#000077");
	AG_SetStyle(win, "color",            "#eeeeee");
	AG_SetStyle(win, "color#hover",      "#aaffaa");
	AG_SetStyle(win, "text-color",       "#000000");
	AG_SetStyle(win, "text-color#hover", "#000077");
	AG_SetStyle(win, "selection-color",  "#00ff00");
	this->nb = AG_NotebookNew(this->win, AG_NOTEBOOK_EXPAND);
}
Lui::~Lui() {
	AG_Destroy();
}

int
Lui::loop() {
	AG_WindowSetGeometryAligned(win, AG_WINDOW_TL, 640, 360);
	AG_WindowShow(this->win);
	//AG_WindowMaximize(win);
	AG_EventLoop();
	return 0;
}
