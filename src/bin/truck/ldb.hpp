
#include <sqlite3.h>


using namespace std;


class Ldb {
private:
protected:
	sqlite3 *db = NULL;
public:
	Ldb(const char *);
	~Ldb();
	void init();
	void eval(string);

	class Stmt {
	protected:
		sqlite3 *const db;
	private:
		sqlite3_stmt *stmt;
		string tbl;
		unsigned int cc = 0;
		queue<Scol> col = {};
		vector<int> ict = {};
	public:
		Stmt(const Ldb *, string, string, string);
		~Stmt();
		int setQueryArg(Sany, Sany);
		int result(queue<Sany> *, queue<Scol> *);
	};
};
